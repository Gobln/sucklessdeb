
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/home/goblin/git/polybar-dwm/src/polybar-msg.cpp" "bin/CMakeFiles/polybar-msg.dir/polybar-msg.cpp.o" "gcc" "bin/CMakeFiles/polybar-msg.dir/polybar-msg.cpp.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/goblin/git/polybar-dwm/build/bin/CMakeFiles/poly.dir/DependInfo.cmake"
  "/home/goblin/git/polybar-dwm/build/lib/xpp/CMakeFiles/xpp.dir/DependInfo.cmake"
  "/home/goblin/git/polybar-dwm/build/lib/i3ipcpp/CMakeFiles/i3ipc++.dir/DependInfo.cmake"
  "/home/goblin/git/polybar-dwm/build/lib/dwmipcpp/CMakeFiles/dwmipcpp.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
