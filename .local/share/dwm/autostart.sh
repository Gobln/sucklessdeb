#!/bin/bash
nitrogen --restore &
picom --config ~/.config/picom/picom.conf &
sxhkd -c ~/.config/suckless/sxhkd/sxhkdrc &
