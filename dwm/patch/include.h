/* Bar functionality */
#include "bar_indicators.h"
#include "bar_tagicons.h"
#include "bar.h"

#include "bar_alpha.h"
#include "bar_anybar.h"

/* Other patches */
#include "alttab.h"
#include "attachx.h"
#include "autostart.h"
#include "cyclelayouts.h"
#include "focusfollowmouse.h"
#include "fullscreen.h"
#include "ipc.h"
#include "ipc/ipc.h"
#include "ipc/util.h"
#include "restartsig.h"
#include "rotatestack.h"
#include "scratchpad.h"
#include "swallow.h"
/* Layouts */
#include "layout_monocle.h"
#include "layout_tile.h"

